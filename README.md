## To send a Push Notification

curl -X POST -H "Authorization: key=YOUR_SERVER_KEY" -H "Content-Type: application/json" -d '{
  "notification": {
    "title": "TEST MESSAGE PWA",
    "body": "TEST MESSAGE BODY PWA",
  },
  "to": "DEVICE_REGISTRATION_TOKEN"
}' "https://fcm.googleapis.com/fcm/send"

Note : You can retrieve your Firebase server key from Project Settings -> Cloud Messaging section in Firebase console. And Device Registration Token from console log after you allow the notification.