importScripts('https://www.gstatic.com/firebasejs/5.11.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.11.0/firebase-messaging.js');

var config = {
	apiKey: "AIzaSyDXxoMVCKq5gihrQDB7GmH1R8O3kNfWTgg",
	authDomain: "test-pwa-a6b8c.firebaseapp.com",
	databaseURL: "https://test-pwa-a6b8c.firebaseio.com",
	projectId: "test-pwa-a6b8c",
	storageBucket: "test-pwa-a6b8c.appspot.com",
	messagingSenderId: "163797154508"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
	const title = 'Hello World';
	const options = {
		body: payload.data.body
	};
	return self.registration.showNotification(title, options);
});